<?php
namespace SkyBet\TechDemo\Tests;

use PHPUnit\Framework\TestCase as TestCasePresenter;
use PHPUnit_Framework_MockObject_MockObject as MockObject;

class TestCase extends TestCasePresenter
{
    protected $composer;

    public function setUp()
    {
        parent::setUp();

        require_once __DIR__ . '/../boot/autoload.php';
        require_once __DIR__ . '/../boot/constants.php';

        defined('VERBOSE_MODE')
            or define('VERBOSE_MODE', true);

        $this->composer = json_decode(file_get_contents(ROOT . 'composer.json'));
    }

    /**
     * Setup methods required to mock an Iterable thing
     *
     * @param string $className for mocking
     * @param array $items Initial items
     *
     * @return MockObject The iterator mock
     */
    public function createIterableMock(string $className, array $items = [])
    {
        $iterableMock = $this->createMock($className);
        $position = 0;

        $iterableMock->expects($this->any())
            ->method('rewind')
            ->will(
                $this->returnCallback(
                    function() use (&$position) {
                        $position = 0;
                    }
                )
            );

        $iterableMock->expects($this->any())
            ->method('current')
            ->will(
                $this->returnCallback(
                    function() use ($items, $position) {
                        return $items[$position];
                    }
                )
            );

        $iterableMock->expects($this->any())
            ->method('key')
            ->will(
                $this->returnCallback(
                    function() use ($position) {
                        return $position;
                    }
                )
            );

        $iterableMock->expects($this->any())
            ->method('next')
            ->will(
                $this->returnCallback(
                    function() use (&$position) {
                        $position++;
                    }
                )
            );

        $iterableMock->expects($this->any())
            ->method('valid')
            ->will(
                $this->returnCallback(
                    function() use ($items, $position) {
                        return isset($items[$position]);
                    }
                )
            );

        $iterableMock->expects($this->any())
            ->method('count')
            ->will(
                $this->returnCallback(
                    function() use ($items) {
                        return sizeof($items);
                    }
                )
            );

        return $iterableMock;
    }

    /**
     * Setup methods required to mock a collection
     *
     * @param string $className for mocking collections
     * @param array $items Initial items
     *
     * @return MockObject The iterator mock
     */
    public function createCollectionMock(string $className, array $items = [])
    {
        $collectionMock = $this->createIterableMock($className, $items);

        $collectionMock->expects($this->any())
            ->method('add')
            ->will(
                $this->returnCallback(
                    function($item) use (&$items) {
                        $items[] = $item;
                    }
                )
            );

        $collectionMock->expects($this->any())
            ->method('unionWith')
            ->will(
                $this->returnCallback(
                    function($item) use (&$items) {
                        $items[] = $item;
                    }
                )
            );

        return $collectionMock;
    }
}
