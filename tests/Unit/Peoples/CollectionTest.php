<?php
namespace SkyBet\TechDemo\Tests\Unit\Peoples;

use SkyBet\TechDemo\Tests\TestCase;
use PHPUnit_Framework_MockObject_MockObject as MockedObject;
use ReflectionClass;

/**
 * Tests for Peoples\Collection class
 */
class CollectionTest extends TestCase
{
    /** @var ReflectionClass */
    private $class;
    // ------------------------------------------------------------------------------------------------------

    /** @var ReflectionClass */
    private $interface;
    // ------------------------------------------------------------------------------------------------------

    /**
     * Initialize private properties
     */
    public function setUp()
    {
        parent::setUp();

        $this->interface = new ReflectionClass(__ROOT_NAMESPACE__ . 'Abstracts\\Collections\\Peoples');
        $this->class = new ReflectionClass(__ROOT_NAMESPACE__ . 'Peoples\\Collection');
    }
    // ------------------------------------------------------------------------------------------------------

    /**
     * Sanity check: The class implements the expected Interface?
     * @test
     */
    public function isImplementsInterface()
    {
        $this->assertTrue(
            in_array($this->interface->getName(), $this->class->getInterfaceNames()),
            "{$this->class->getName()} doesn't implements {$this->interface->getName()}"
        );
    }
    // ------------------------------------------------------------------------------------------------------

    public function itemProvider()
    {
        $this->setUp();

        $item1 = $this->createMock(__ROOT_NAMESPACE__ . 'Abstracts\\Entities\\People');
        $item1->expects($this->atLeastOnce())->method('id')->willReturn(1);

        return [
            'I1'    => [$item1],
        ];
    }

    /**
     * Just check if it can receive an item and it can retrieve it
     *
     * @param MockedObject $item
     * @return \SkyBet\TechDemo\Abstracts\Collection
     *
     * @dataProvider itemProvider
     * @depends isImplementsInterface
     * @test
     */
    public function canReceiveItem(MockedObject $item)
    {
        /** @var \SkyBet\TechDemo\Abstracts\Collection $instance */
        $instance = $this->class->newInstance();

        /** @var \SkyBet\TechDemo\Abstracts\Entity $item */
        $instance->add($item);

        // Sanity check: it has only one item?
        $this->assertCount(1, $instance);

        // Then this will only iterate once
        foreach($instance as $retrievedItem)
            $this->assertSame($item, $retrievedItem);

        return $instance;
    }

    /**
     * Just check if it can receive an item and it can retrieve it
     *
     * @depends canReceiveItem
     * @test
     */
    public function canDetectContaining()
    {
        /** @var \SkyBet\TechDemo\Abstracts\Collection $instance */
        $instance = $this->class->newInstance();

        $item = $this->createMock(__ROOT_NAMESPACE__ . 'Abstracts\\Entities\\People');
        $item->expects($this->atLeastOnce())->method('id')->willReturn(1);

        /** @var \SkyBet\TechDemo\Abstracts\Entity $item */
        $instance->add($item);

        $this->assertTrue($instance->containing($item));
    }

    /**
     * Just check if it can receive an item and it can retrieve it
     *
     * @depends canReceiveItem
     * @test
     */
    public function canRemoveItem()
    {
        $item = $this->createMock(__ROOT_NAMESPACE__ . 'Abstracts\\Entities\\People');
        $item->expects($this->atLeastOnce())->method('id')->willReturn(1);

        $instance = $this->canReceiveItem($item);

        /** @var \SkyBet\TechDemo\Abstracts\Entity $item */
        $instance->remove($item);

        // There is no other item left?
        $this->assertCount(0, $instance);
    }
    // ------------------------------------------------------------------------------------------------------

    public function unionProvider()
    {
        $this->setUp();

        $item1 = $this->createMock(__ROOT_NAMESPACE__ . 'Abstracts\\Entities\\People');
        $item1->expects($this->atLeastOnce())->method('id')->willReturn(1);
        /** @var \SkyBet\TechDemo\Abstracts\Entity $item1 */

        $item2 = $this->createMock(__ROOT_NAMESPACE__ . 'Abstracts\\Entities\\People');
        $item2->expects($this->atLeastOnce())->method('id')->willReturn(2);
        /** @var \SkyBet\TechDemo\Abstracts\Entity $item2 */

        $item3 = $this->createMock(__ROOT_NAMESPACE__ . 'Abstracts\\Entities\\People');
        $item3->expects($this->atLeastOnce())->method('id')->willReturn(3);
        /** @var \SkyBet\TechDemo\Abstracts\Entity $item3 */

        $className = $this->class->getName();

        /** @var \SkyBet\TechDemo\Abstracts\Collection $collection1 */
        $collection1 = new $className();
        $collection1->add($item1);

        /** @var \SkyBet\TechDemo\Abstracts\Collection $collection2 */
        $collection2 = new $className();
        $collection2->add($item1);
        $collection2->add($item2);

        /** @var \SkyBet\TechDemo\Abstracts\Collection $collection3 */
        $collection3 = new $className();
        $collection3->add($item2);

        /** @var \SkyBet\TechDemo\Abstracts\Collection $collection4 */
        $collection4 = new $className();
        $collection4->add($item2);
        $collection4->add($item3);

        /** @var \SkyBet\TechDemo\Abstracts\Collection $collection5 */
        $collection5 = new $className();
        $collection5->add($item3);

        return [
            '(I1)+(I1+I2)'        => [clone $collection1, clone $collection2, [$item1, $item2]],
            '(I1)+(I2)'           => [clone $collection1, clone $collection3, [$item1, $item2]],
            '(I1)+(I2+I3)'        => [clone $collection1, clone $collection4, [$item1, $item2, $item3]],
            '(I1)+(I3)'           => [clone $collection1, clone $collection5, [$item1, $item3]],

            '(I1+I2)+(I1)'        => [clone $collection2, clone $collection1, [$item1, $item2]],
            '(I1+I2)+(I2)'        => [clone $collection2, clone $collection3, [$item1, $item2]],
            '(I1+I2)+(I2+I3)'     => [clone $collection2, clone $collection4, [$item1, $item2, $item3]],
            '(I1+I2)+(I3)'        => [clone $collection2, clone $collection5, [$item1, $item2, $item3]],

            '(I2)+(I1)'           => [clone $collection3, clone $collection1, [$item1, $item2]],
            '(I2)+(I1+I2)'        => [clone $collection3, clone $collection2, [$item1, $item2]],
            '(I2)+(I2+I3)'        => [clone $collection3, clone $collection4, [$item2, $item3]],
            '(I2)+(I3)'           => [clone $collection3, clone $collection5, [$item2, $item3]],

            '(I2+I3)+(I1)'        => [clone $collection4, clone $collection1, [$item1, $item2, $item3]],
            '(I2+I3)+(I1+I2)'     => [clone $collection4, clone $collection2, [$item1, $item2, $item3]],
            '(I2+I3)+(I2)'        => [clone $collection4, clone $collection3, [$item2, $item3]],
            '(I2+I3)+(I3)'        => [clone $collection4, clone $collection5, [$item2, $item3]],

            '(I3)+(I1)'           => [clone $collection5, clone $collection1, [$item1, $item3]],
            '(I3)+(I1+I2)'        => [clone $collection5, clone $collection2, [$item1, $item2, $item3]],
            '(I3)+(I2)'           => [clone $collection5, clone $collection3, [$item2, $item3]],
            '(I3)+(I2+I3)'        => [clone $collection5, clone $collection4, [$item2, $item3]],
        ];
    }

    /**
     * Checking that it can merge it's own Type
     *
     * @param \SkyBet\TechDemo\Abstracts\Collection $a
     * @param \SkyBet\TechDemo\Abstracts\Collection $b
     * @param array $expectedResult
     *
     * @dataProvider unionProvider
     * @depends isImplementsInterface
     * @test
     */
    public function canDoUnion($a, $b, array $expectedResult = [])
    {
        $a->unionWith($b);

        if (empty($expectedResult)) $this->assertEmpty($expectedResult);
        else
        {
            $expectedResultItems = [];
            $resultItems = [];

            foreach($a as $item) $resultItems[] = 'I' . $item->id();
            foreach($expectedResult as $item) $expectedResultItems[] = 'I' . $item->id();

            sort($resultItems);
            sort($expectedResultItems);

            $this->assertSame(
                implode('+', $expectedResultItems),
                implode('+', $resultItems)
            );
        }
    }
    // ------------------------------------------------------------------------------------------------------

    public function subtractProvider()
    {
        $this->setUp();

        $item1 = $this->createMock(__ROOT_NAMESPACE__ . 'Abstracts\\Entities\\People');
        $item1->expects($this->atLeastOnce())->method('id')->willReturn(1);
        /** @var \SkyBet\TechDemo\Abstracts\Entity $item1 */

        $item2 = $this->createMock(__ROOT_NAMESPACE__ . 'Abstracts\\Entities\\People');
        $item2->expects($this->atLeastOnce())->method('id')->willReturn(2);
        /** @var \SkyBet\TechDemo\Abstracts\Entity $item2 */

        $item3 = $this->createMock(__ROOT_NAMESPACE__ . 'Abstracts\\Entities\\People');
        $item3->expects($this->atLeastOnce())->method('id')->willReturn(3);
        /** @var \SkyBet\TechDemo\Abstracts\Entity $item3 */

        /** @var \SkyBet\TechDemo\Abstracts\Collection $collection1 */
        $collection1 = $this->class->newInstance();
        $collection1->add($item1);

        /** @var \SkyBet\TechDemo\Abstracts\Collection $collection2 */
        $collection2 = $this->class->newInstance();
        $collection2->add($item1);
        $collection2->add($item2);

        /** @var \SkyBet\TechDemo\Abstracts\Collection $collection3 */
        $collection3 = $this->class->newInstance();
        $collection3->add($item2);

        /** @var \SkyBet\TechDemo\Abstracts\Collection $collection4 */
        $collection4 = $this->class->newInstance();
        $collection4->add($item2);
        $collection4->add($item3);

        /** @var \SkyBet\TechDemo\Abstracts\Collection $collection4 */
        $collection5 = $this->class->newInstance();
        $collection5->add($item3);

        return [
            '(I1)/(I1+I2)'        => [clone $collection1, clone $collection2, []],
            '(I1)/(I2)'           => [clone $collection1, clone $collection3, [$item1]],
            '(I1)/(I2+I3)'        => [clone $collection1, clone $collection4, [$item1]],
            '(I1)/(I3)'           => [clone $collection1, clone $collection5, [$item1]],

            '(I1+I2)/(I1)'        => [clone $collection2, clone $collection1, [$item2]],
            '(I1+I2)/(I2)'        => [clone $collection2, clone $collection3, [$item1]],
            '(I1+I2)/(I2+I3)'     => [clone $collection2, clone $collection4, [$item1]],
            '(I1+I2)/(I3)'        => [clone $collection2, clone $collection5, [$item1, $item2]],

            '(I2)/(I1)'           => [clone $collection3, clone $collection1, [$item2]],
            '(I2)/(I1+I2)'        => [clone $collection3, clone $collection2, []],
            '(I2)/(I2+I3)'        => [clone $collection3, clone $collection4, []],
            '(I2)/(I3)'           => [clone $collection3, clone $collection5, [$item2]],

            '(I2+I3)/(I1)'        => [clone $collection4, clone $collection1, [$item2, $item3]],
            '(I2+I3)/(I1+I2)'     => [clone $collection4, clone $collection2, [$item3]],
            '(I2+I3)/(I2)'        => [clone $collection4, clone $collection3, [$item3]],
            '(I2+I3)/(I3)'        => [clone $collection4, clone $collection5, [$item2]],

            '(I3)/(I1)'           => [clone $collection5, clone $collection1, [$item3]],
            '(I3)/(I1+I2)'        => [clone $collection5, clone $collection2, [$item3]],
            '(I3)/(I2)'           => [clone $collection5, clone $collection3, [$item3]],
            '(I3)/(I2+I3)'        => [clone $collection5, clone $collection4, []],
        ];
    }

    /**
     * Checking that it can subtract items by a collection
     *
     * @param \SkyBet\TechDemo\Abstracts\Collection $a
     * @param \SkyBet\TechDemo\Abstracts\Collection $b
     * @param array $expectedResult
     *
     * @dataProvider subtractProvider
     * @depends isImplementsInterface
     * @test
     */
    public function canDoSubtract($a, $b, array $expectedResult)
    {
        $a->subtractWith($b);

        if (empty($expectedResult)) $this->assertEmpty($expectedResult);
        else
        {
            $expectedResultItems = [];
            $resultItems = [];

            foreach($a as $item) $resultItems[] = 'I' . $item->id();
            foreach($expectedResult as $item) $expectedResultItems[] = 'I' . $item->id();

            sort($resultItems);
            sort($expectedResultItems);

            $this->assertSame(
                implode('+', $expectedResultItems),
                implode('+', $resultItems)
            );
        }
    }
    // ------------------------------------------------------------------------------------------------------

    public function intersectProvider()
    {
        $this->setUp();

        $item1 = $this->createMock(__ROOT_NAMESPACE__ . 'Abstracts\\Entities\\People');
        $item1->expects($this->atLeastOnce())->method('id')->willReturn(1);
        /** @var \SkyBet\TechDemo\Abstracts\Entity $item1 */

        $item2 = $this->createMock(__ROOT_NAMESPACE__ . 'Abstracts\\Entities\\People');
        $item2->expects($this->atLeastOnce())->method('id')->willReturn(2);
        /** @var \SkyBet\TechDemo\Abstracts\Entity $item2 */

        $item3 = $this->createMock(__ROOT_NAMESPACE__ . 'Abstracts\\Entities\\People');
        $item3->expects($this->atLeastOnce())->method('id')->willReturn(3);
        /** @var \SkyBet\TechDemo\Abstracts\Entity $item3 */

        /** @var \SkyBet\TechDemo\Abstracts\Collection $collection1 */
        $collection1 = $this->class->newInstance();
        $collection1->add($item1);

        /** @var \SkyBet\TechDemo\Abstracts\Collection $collection2 */
        $collection2 = $this->class->newInstance();
        $collection2->add($item1);
        $collection2->add($item2);

        /** @var \SkyBet\TechDemo\Abstracts\Collection $collection3 */
        $collection3 = $this->class->newInstance();
        $collection3->add($item2);

        /** @var \SkyBet\TechDemo\Abstracts\Collection $collection4 */
        $collection4 = $this->class->newInstance();
        $collection4->add($item2);
        $collection4->add($item3);

        /** @var \SkyBet\TechDemo\Abstracts\Collection $collection5 */
        $collection5 = $this->class->newInstance();
        $collection5->add($item3);

        return [
            '(I1)^(I1+I2)'        => [clone $collection1, clone $collection2, [$item1]],
            '(I1)^(I2)'           => [clone $collection1, clone $collection3, []],
            '(I1)^(I2+I3)'        => [clone $collection1, clone $collection4, []],
            '(I1)^(I3)'           => [clone $collection1, clone $collection5, []],

            '(I1+I2)^(I1)'        => [clone $collection2, clone $collection1, [$item1]],
            '(I1+I2)^(I2)'        => [clone $collection2, clone $collection3, [$item2]],
            '(I1+I2)^(I2+I3)'     => [clone $collection2, clone $collection4, [$item2]],
            '(I1+I2)^(I3)'        => [clone $collection2, clone $collection5, []],

            '(I2)^(I1)'           => [clone $collection3, clone $collection1, []],
            '(I2)^(I1+I2)'        => [clone $collection3, clone $collection2, [$item2]],
            '(I2)^(I2+I3)'        => [clone $collection3, clone $collection4, [$item2]],
            '(I2)^(I3)'           => [clone $collection3, clone $collection5, []],

            '(I2+I3)^(I1)'        => [clone $collection4, clone $collection1, []],
            '(I2+I3)^(I1+I2)'     => [clone $collection4, clone $collection2, [$item2]],
            '(I2+I3)^(I2)'        => [clone $collection4, clone $collection3, [$item2]],
            '(I2+I3)^(I3)'        => [clone $collection4, clone $collection5, [$item3]],

            '(I3)^(I1)'           => [clone $collection5, clone $collection1, []],
            '(I3)^(I1+I2)'        => [clone $collection5, clone $collection2, []],
            '(I3)^(I2)'           => [clone $collection5, clone $collection3, []],
            '(I3)^(I2+I3)'        => [clone $collection5, clone $collection4, [$item3]],
        ];
    }

    /**
     *
     * @param \SkyBet\TechDemo\Abstracts\Collection $a
     * @param \SkyBet\TechDemo\Abstracts\Collection $b
     * @param array $expectedResult
     *
     * @dataProvider intersectProvider
     * @depends isImplementsInterface
     * @test
     */
    public function canDoIntersect($a, $b, array $expectedResult)
    {
        $a->intersectWith($b);

        if (empty($expectedResult)) $this->assertEmpty($expectedResult);
        else
        {
            $expectedResultItems = [];
            $resultItems = [];

            foreach($a as $item) $resultItems[] = 'I' . $item->id();
            foreach($expectedResult as $item) $expectedResultItems[] = 'I' . $item->id();

            sort($resultItems);
            sort($expectedResultItems);

            $this->assertSame(
                implode('+', $expectedResultItems),
                implode('+', $resultItems)
            );
        }
    }
    // ------------------------------------------------------------------------------------------------------

    public function typeCheckProvider()
    {
        return [
            'Add() Valid'   =>  ['add', null, __ROOT_NAMESPACE__ . 'Abstracts\\Entities\\People'],
            'Add() Invalid'   =>  ['add', 'InvalidArgumentException', __ROOT_NAMESPACE__ . 'Abstracts\\Entity'],
            'Remove() Valid'   =>  ['remove', null, __ROOT_NAMESPACE__ . 'Abstracts\\Entities\\People'],
            'Remove() Invalid'   =>  ['remove', 'InvalidArgumentException', __ROOT_NAMESPACE__ . 'Abstracts\\Entity'],
        ];
    }

    /**
     * @param $methodName
     * @param $exceptionName
     * @param $itemName
     *
     * @dataProvider typeCheckProvider
     * @test
     */
    public function isCheckingType($methodName, $exceptionName, $itemName)
    {
        $itemMock = $this->createMock($itemName);

        /** @var \SkyBet\TechDemo\Abstracts\Collection $instance */
        $instance = $this->class->newInstance();

        if (!empty($exceptionName)) $this->expectException($exceptionName);
        $instance->$methodName($itemMock);

        $this->assertTrue(true);
    }
}
