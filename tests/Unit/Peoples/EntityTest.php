<?php
namespace SkyBet\TechDemo\Tests\Unit\Peoples;

use SkyBet\TechDemo\Tests\TestCase;
use ReflectionClass;

/**
 * Test for Peoples\People Entity class
 */
class EntityTest extends TestCase
{
    /** @var ReflectionClass */
    private $class;
    // ------------------------------------------------------------------------------------------------------

    /** @var ReflectionClass */
    private $interface;
    // ------------------------------------------------------------------------------------------------------

    /**
     * Initialize private properties
     */
    public function setUp()
    {
        parent::setUp();

        $this->interface = new ReflectionClass(__ROOT_NAMESPACE__ . 'Abstracts\\Entities\\People');
        $this->class = new ReflectionClass(__ROOT_NAMESPACE__ . 'Peoples\\People');
    }
    // ------------------------------------------------------------------------------------------------------

    /**
     * Sanity check: The class implements the expected Interface?
     * @test
     */
    public function isImplementsInterface()
    {
        $this->assertTrue(
            in_array($this->interface->getName(), $this->class->getInterfaceNames()),
            "{$this->class->getName()} doesn't implements {$this->interface->getName()}"
        );
    }
    // ------------------------------------------------------------------------------------------------------

    /**
     * Provider for Constructor
     * @return array
     */
    public function initializationDataProvider()
    {
        return [
            'Valid #1'      => [
                null,
                (array) ['id' => 1, 'forName' => "Jeff", 'lastName' => "Stelling"]
            ],
            'Valid #2'      => [
                null,
                (object) ['id' => 2, 'forName' => "Chris", 'lastName' => "Kamara"]
            ],
            'Invalid #1'    => [
                'InvalidArgumentException',
                json_encode(['id' => 3, 'forName' => "Alex", 'lastName' => "Hammond"])
            ],
            'Invalid #2'    => [
                'InvalidArgumentException',
                serialize(['id' => 4, 'forName' => "Jim", 'lastName' => "White"])
            ],
            'Invalid #3'    => [
                'DomainException',
                ['id' => 5, 'forName' => "Natalie"]
            ],
            'Invalid #4'    => [
                'InvalidArgumentException',
                ['id' => 6, 'name' => 'Rick Sanchez']
            ],
        ];
    }

    /**
     * Can initialize with the given dataset?
     *
     * @param null|string $expectedException
     * @param $dataset
     *
     * @dataProvider initializationDataProvider
     * @depends isImplementsInterface
     * @test
     */
    public function canInitializeWithDataset(?string $expectedException, $dataset)
    {
        $className = $this->class->getName();
        if (!empty($expectedException)) $this->expectException($expectedException);
        $instance = new $className($dataset);

        foreach ($dataset as $key => $value)
            $this->assertSame($instance->$key(), $value);
    }
    // ------------------------------------------------------------------------------------------------------
}
