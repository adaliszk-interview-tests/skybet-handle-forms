<?php
namespace SkyBet\TechDemo\Tests\Unit\Peoples;

use SkyBet\TechDemo\Abstracts\Registry;
use PHPUnit_Framework_MockObject_MockObject as MockObject;
use SkyBet\TechDemo\Tests\TestCase;
use ReflectionMethod;
use ReflectionClass;

class RegistryTest extends TestCase
{
    /** @var ReflectionClass */
    private $class;
    // ------------------------------------------------------------------------------------------------------

    /** @var ReflectionClass */
    private $interface;
    // ------------------------------------------------------------------------------------------------------

    /**
     * Initialize private properties
     */
    public function setUp()
    {
        parent::setUp();

        $this->class = new ReflectionClass(__ROOT_NAMESPACE__ . 'Peoples\\Registry');
        $this->interface = new ReflectionClass(__ROOT_NAMESPACE__ . 'Abstracts\\Registries\\Peoples');
    }
    // ------------------------------------------------------------------------------------------------------

    /**
     * Sanity check: The class implements the expected Interface?
     * @test
     */
    public function isImplementsInterface()
    {
        $this->assertTrue(
            in_array($this->interface->getName(), $this->class->getInterfaceNames()),
            "{$this->class->getName()} doesn't implements {$this->interface->getName()}"
        );
    }
    // ------------------------------------------------------------------------------------------------------

    public function dependencyProvider()
    {
        $this->setUp();

        $dataSourceClassName = __ROOT_NAMESPACE__ . 'Abstracts\\DataSource';
        $collectionClassName = __ROOT_NAMESPACE__ . 'Abstracts\\Collections\\Peoples';
        $entityClassName = __ROOT_NAMESPACE__ . 'Abstracts\\Entities\\People';

        return [
            'DataSource'    =>  [$dataSourceClassName],
            'Collection'    =>  [$collectionClassName],
            'Entity'        =>  [$entityClassName],
        ];
    }

    /**
     * Sanity check: The class getting it's dependencies?
     *
     * @param string $dependencyName
     *
     * @dataProvider dependencyProvider
     * @depends isImplementsInterface
     * @test
     */
    public function hasExpectedDependency(string $dependencyName)
    {
        $dependencies = [];
        foreach ($this->class->getConstructor()->getParameters() as $d)
            $dependencies[] = $d->getClass()->name;

        $this->assertTrue(
            in_array($dependencyName, $dependencies),
            "{$this->class->getName()} doesn't has dependency of {$dependencyName}"
        );
    }
    // ------------------------------------------------------------------------------------------------------

    /**
     *  Check that the register can initialize
     *
     * @depends hasExpectedDependency
     * @test
     */
    public function canInitialize()
    {
        $dependencyNames = $this->dependencyProvider();

        $dependencies = [];
        foreach ($dependencyNames as $name => $className)
            $dependencies[] = $this->createMock($className[0]);

        $this->class->newInstance(...$dependencies);

        // If it's able to reach this point then it can initialize
        $this->assertTrue(true);
    }
    // ------------------------------------------------------------------------------------------------------

    public function sampleDataProvider()
    {
        return [
            'Valid #1'      => [
                null,
                (array) ['id' => 1, 'forName' => "Jeff", 'lastName' => "Stelling"],
                'getById',
                [1],
                ['id' => 1, 'forName' => "Jeff", 'lastName' => "Stelling"]
            ],
            'Valid #2'      => [
                null,
                (object) ['id' => 2, 'forName' => "Chris", 'lastName' => "Kamara"],
                'getById',
                [2],
                ['id' => 2, 'forName' => "Chris", 'lastName' => "Kamara"]
            ],
            'Valid #3'      => [
                null,
                [
                    ['id' => 1, 'forName' => "Jeff", 'lastName' => "Stelling"],
                    ['id' => 2, 'forName' => "Chris", 'lastName' => "Kamara"],
                    ['id' => 3, 'forName' => "Alex", 'lastName' => "Hammond"],
                    ['id' => 4, 'forName' => "Jim", 'lastName' => "White"],
                    ['id' => 5, 'forName' => "Natalie", 'lastName' => "Sawyer"]
                ],
                'getAll',
                [],
                [
                    ['id' => 1, 'forName' => "Jeff", 'lastName' => "Stelling"],
                    ['id' => 2, 'forName' => "Chris", 'lastName' => "Kamara"],
                    ['id' => 3, 'forName' => "Alex", 'lastName' => "Hammond"],
                    ['id' => 4, 'forName' => "Jim", 'lastName' => "White"],
                    ['id' => 5, 'forName' => "Natalie", 'lastName' => "Sawyer"]
                ],
            ],
            'Invalid #1'    => [
                'InvalidArgumentException',
                json_encode(['id' => 3, 'forName' => "Alex", 'lastName' => "Hammond"]),
                null,
                [],
                [],
            ],
            'Invalid #2'    => [
                'InvalidArgumentException',
                serialize(['id' => 4, 'forName' => "Jim", 'lastName' => "White"]),
                null,
                [],
                [],
            ],
            'Invalid #3'    => [
                'DomainException',
                ['id' => 5, 'forName' => "Natalie"],
                null,
                [],
                [],
            ],
            'Invalid #4'    => [
                'InvalidArgumentException',
                ['id' => 6, 'name' => 'Rick Sanchez'],
                null,
                [],
                [],
            ],
        ];
    }

    /**
     * Registry can get Entries by ID?
     *
     * @param null|string $exceptionName
     * @param iterable|object $data
     * @param null|string $method
     * @param array $params
     * @param array $resultItems
     *
     * @dataProvider sampleDataProvider
     * @depends hasExpectedDependency
     * @test
     */
    public function canRetrieveEntities(
        ?string $exceptionName,
        $data,
        ?string $method,
        array $params,
        array $resultItems
    ) {
        $dependencyNames = $this->dependencyProvider();

        $dependencies = [];
        foreach ($dependencyNames as $name => $className)
        {
            switch ($name)
            {
                case "DataSource":

                    $getContentReturn = $this->createIterableMock(
                        __ROOT_NAMESPACE__ . 'Abstracts\\Data',
                        (array) $data
                    );

                    $dataMock = $this->createMock($className[0]);
                    $dataMock->expects($this->atLeastOnce())
                             ->method('getContent')
                             ->willReturn($getContentReturn);

                    $dependencies[] = $dataMock;
                    break;

                default:
                    $dependencies[] = $this->createMock($className[0]);
                    break;
            }
        }

        // If I wait some exception to happen?
        if (!empty($exceptionName)) $this->expectException($exceptionName);

        $instance = $this->class->newInstance(...$dependencies);
        $result = $instance->$method(...$params);

        $resultCollection = $this->createCollectionMock(
            __ROOT_NAMESPACE__ . 'Abstracts\\Collections\\Peoples',
            $resultItems
        );

        $this->assertSame($result, $resultCollection);
    }
}
