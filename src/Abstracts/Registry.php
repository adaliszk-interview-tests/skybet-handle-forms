<?php
namespace SkyBet\TechDemo\Abstracts;

/**
 * Simple Registry Interface
 */
interface Registry
{
    /**
     * Getting one Entity by ID
     *
     * @param int $id
     * @return Entity
     */
    public function getById(int $id): Entity;

    /**
     * Getting Entities from Registry
     *
     * @return Collection
     */
    public function getAll(): Collection;
}
