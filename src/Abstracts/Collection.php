<?php
namespace SkyBet\TechDemo\Abstracts;

use Serializable;
use ArrayAccess;
use Countable;
use Iterator;

/**
 * Basic Collection Interface
 */
interface Collection extends Serializable, ArrayAccess, Countable, Iterator
{
    /**
     * Add item to the collection
     *
     * @param Entity $item
     * @param mixed $data
     */
    public function add(Entity $item, $data = null): void;

    /**
     * Remove item from the collection
     *
     * @param Entity $item
     */
    public function remove(Entity $item): void;

    /**
     * Checks that an Item is already in the collection
     *
     * @param Entity $item
     * @return bool
     */
    public function containing(Entity $item): bool;

    /**
     * Make Union with a given collection and the result would be
     * the base Collection having all of the items
     *
     * @param Collection $collection
     */
    public function unionWith(Collection $collection): void;

    /**
     * Subtract items from the collections using an other collection
     *
     * @param Collection $collection
     */
    public function subtractWith(Collection $collection): void;

    /**
     * Subtract items from the collection but keeping the given
     * collection items if they are in the base collection
     *
     * @param Collection $collection
     */
    public function intersectWith(Collection $collection): void;

    /**
     * Creating a new instance from the same Collection
     *
     * This function is needed for testing porpoises, you cannot create with the new keyword because
     * it looses the mocked methods sadly.
     *
     * @return Collection
     */
    public function newInstance(): Collection;
}
