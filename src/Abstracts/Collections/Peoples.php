<?php
namespace SkyBet\TechDemo\Abstracts\Collections;

use SkyBet\TechDemo\Abstracts\Collection;
use SkyBet\TechDemo\Abstracts\Entities\People as PeopleEntity;
use SkyBet\TechDemo\Abstracts\Entity;

/**
 * People Collection Interface
 */
interface Peoples extends Collection
{
    /**
     * Add item to the Collection, but it has to check for type!
     *
     * NOTE: You have to do the type check manually because inheritance not working properly
     *       in interfaces: https://bugs.php.net/bug.php?id=75095
     *
     * @param PeopleEntity|Entity $item
     * @param mixed $data
     */
    public function add(Entity $item, $data = null): void;

    /**
     * Remove item to the Collection, but it has to check for type!
     *
     * NOTE: You have to do the type check manually because inheritance not working properly
     *       in interfaces: https://bugs.php.net/bug.php?id=75095
     *
     * @param PeopleEntity|Entity $item
     */
    public function remove(Entity $item): void;
}
