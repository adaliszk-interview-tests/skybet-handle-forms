<?php
namespace SkyBet\TechDemo\Abstracts;

/**
 * Basic Entity Interface
 */
interface Entity
{
    /**
     * Getter for ID property
     * @return int
     */
    public function id(): int;

    /**
     * Creating a new instance from the same Entity
     *
     * This function is needed for testing porpoises, you cannot create with the new keyword because
     * it looses the mocked methods sadly.
     *
     * @param iterable|null $initialData
     * @return Entity
     */
    public function newInstance(?iterable $initialData = null): Entity;
}
