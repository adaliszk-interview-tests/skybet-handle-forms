<?php
namespace SkyBet\TechDemo\Abstracts;

use ArrayAccess;
use Iterator;
use Countable;

interface Data extends Iterator, ArrayAccess, Countable
{
    // Nothing to do here
}
