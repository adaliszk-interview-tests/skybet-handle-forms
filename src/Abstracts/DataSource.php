<?php
namespace SkyBet\TechDemo\Abstracts;

interface DataSource
{
    /**
     * Open a connection to the sourceName which can be a filename or a collection name
     *
     * @param string $sourceFilename
     * @return bool
     */
    public function open(string $sourceFilename): bool;

    /**
     * Get the file content from the given format
     *
     * @return Data
     */
    public function getContent(): Data;
}
