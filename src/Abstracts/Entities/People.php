<?php
namespace SkyBet\TechDemo\Abstracts\Entities;

use SkyBet\TechDemo\Abstracts\Entity;

/**
 * People Entity
 */
interface People extends Entity
{
    /**
     * Getter for the FORNAME property
     * @return string
     */
    public function forName(): string;

    /**
     * Getter for the LASTNAME property
     * @return string
     */
    public function lastName(): string;
}
