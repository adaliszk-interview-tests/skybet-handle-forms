<?php
namespace SkyBet\TechDemo\Abstracts\Registries;

use SkyBet\TechDemo\Abstracts\Collections\Peoples as PeopleCollection;
use SkyBet\TechDemo\Abstracts\Registry;

/**
 * Peoples Registry
 */
interface Peoples extends Registry
{
    // Nothing special here...
}
