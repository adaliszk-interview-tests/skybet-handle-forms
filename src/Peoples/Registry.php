<?php
namespace SkyBet\TechDemo\Peoples;

use SkyBet\TechDemo\Abstracts\DataSource as DataSourceInterface;
use SkyBet\TechDemo\Abstracts\Collection as CollectionInterface;
use SkyBet\TechDemo\Abstracts\Collections\Peoples as PeopleCollectionInterface;
use SkyBet\TechDemo\Abstracts\Entities\People as PeopleInterface;
use SkyBet\TechDemo\Abstracts\Entity as EntityInterface;

use SkyBet\TechDemo\Abstracts\Registries\Peoples as PeoplesRegistryInterface;
use SkyBet\TechDemo\Providers\Registry as RegistryProvider;
use ReflectionClass;

class Registry extends RegistryProvider implements PeoplesRegistryInterface
{
    /** @var PeopleCollectionInterface */
    private $collection;

    /** @var PeopleInterface */
    private $entity;

    /** @var PeopleInterface[]|PeopleCollectionInterface */
    protected $entities = [];

    /** @var PeopleInterface[] */
    protected $ids = [];

    /** @var PeopleInterface[] */
    protected $forNames = [];

    /** @var PeopleInterface[] */
    protected $lastNames = [];

    /**
     * Get the Dependencies
     *
     * @param DataSourceInterface $dataSource
     * @param PeopleCollectionInterface $collection
     * @param PeopleInterface $entity
     * @internal param string $storeFilename
     * @internal param DataSourceInterface $database
     */
    public function __construct(
        DataSourceInterface $dataSource,
        PeopleCollectionInterface $collection,
        PeopleInterface $entity
    ) {
        parent::__construct($dataSource, 'peoples');

        $this->collection = $collection;
        $this->entity = $entity;

        $this->initializeData();
    }

    /**
     * Initialize data from DB
     */
    protected function initializeData()
    {
        foreach ($this->db->getContent() as $item)
            $this->entities[] = $this->entity->newInstance($item);
    }

    /**
     * Initialize mappings for quick access
     */
    protected function initializeMappings()
    {
        $entities = $this->entities;
        $this->entities = $this->collection->newInstance();

        foreach ($entities as $entity)
        {
            //$this->ids = $this->collection->newInstance();
            //$this->forNames = $this->collection->newInstance();
            //$this->lastNames = $this->collection->newInstance();


            $this->entities->add($entity);
        }

    }

    /**
     * Basic query for an Entity
     *
     * @param int $id
     * @return PeopleInterface|EntityInterface
     */
    public function getById(int $id): EntityInterface
    {
        // TODO: Implement getById() method.
    }

    /**
     * Get all Results
     *
     * @return PeopleCollectionInterface|CollectionInterface
     */
    public function getAll(): CollectionInterface
    {
        // TODO: Implement get() method.
    }
}