<?php
namespace SkyBet\TechDemo\Peoples;

use SkyBet\TechDemo\Abstracts\Data as DataInterface;
use SkyBet\TechDemo\Abstracts\Entity as EntityInterface;
use SkyBet\TechDemo\Providers\Entity as EntityProvider;
use SkyBet\TechDemo\Abstracts\Entities\People as PeopleInterface;

/**
 * People Entity Class
 */
class People extends EntityProvider implements PeopleInterface
{
    /** @var array of required properties for fill with data */
    protected $fillable = ['id', 'forName', 'lastName'];

    /** @var string */
    protected $forName;

    /** @var string */
    protected $lastName;

    /**
     * Initialize Dependencies and Properties
     * @param iterable|object|null $initialData
     */
    public function __construct($initialData = null)
    {
        if (!empty($initialData)) $this->initializeProperties($initialData);
    }

    /**
     * Getter for the FORNAME property
     * @return string
     */
    public function forName(): string
    {
        return $this->forName;
    }

    /**
     * Getter for the LASTNAME property
     * @return string
     */
    public function lastName(): string
    {
        return $this->lastName;
    }

    /**
     * Creating a new instance from the same Entity
     *
     * This function is needed for testing porpoises, you cannot create with the new keyword because
     * it looses the mocked methods sadly.
     *
     * @param iterable|null $initialData
     * @return EntityInterface
     */
    public function newInstance(?iterable $initialData = null): EntityInterface
    {
        return new self($initialData);
    }
}
