<?php
namespace SkyBet\TechDemo\Peoples;

use SkyBet\TechDemo\Abstracts\Collections\Peoples as PeopleCollectionInterface;
use SkyBet\TechDemo\Abstracts\Entities\People as PeopleInterface;
use SkyBet\TechDemo\Abstracts\Collection as CollectionInterface;
use SkyBet\TechDemo\Abstracts\Entity as EntityInterface;
use SkyBet\TechDemo\Providers\Collection as CollectionProvider;
use InvalidArgumentException;

/**
 * Peoples/Collection class
 */
class Collection extends CollectionProvider implements PeopleCollectionInterface
{
    /**
     * Add item to the Collection, but it has to check for type!
     *
     * NOTE: You have to do the type check manually because inheritance not working properly
     *       in interfaces: https://bugs.php.net/bug.php?id=75095
     *
     * @param PeopleInterface|EntityInterface $item
     * @param mixed $data
     */
    public function add(EntityInterface $item, $data = null): void
    {
        if (!($item instanceof PeopleInterface))
            throw new InvalidArgumentException('Item is not instance of ' . PeopleInterface::class);

        $this->attach($item, $data);
    }

    /**
     * Remove item to the Collection, but it has to check for type!
     *
     * NOTE: You have to do the type check manually because inheritance not working properly
     *       in interfaces: https://bugs.php.net/bug.php?id=75095
     *
     * @param PeopleInterface|EntityInterface $item
     */
    public function remove(EntityInterface $item): void
    {
        if (!($item instanceof PeopleInterface))
            throw new InvalidArgumentException('Item is not instance of ' . PeopleInterface::class);

        $this->detach($item);
    }

    /**
     * Creating a new instance from the same Collection
     *
     * This function is needed for testing porpoises, you cannot create with the new keyword because
     * it looses the mocked methods sadly.
     *
     * @return CollectionInterface
     */
    public function newInstance(): CollectionInterface
    {
        return new self();
    }
}
