<?php
namespace SkyBet\TechDemo\Providers;

use SkyBet\TechDemo\Abstracts\Collection as CollectionInterface;
use SkyBet\TechDemo\Abstracts\Entity as ItemInterface;
use SplObjectStorage as CollectionStorageEngine;

/**
 * Just a wrapper around SplObjectStorage so I could use it as abstract
 * sadly it doesn't has proper abstract methods yet.
 */
abstract class Collection extends CollectionStorageEngine implements CollectionInterface
{
    /**
     * Add item to the collection
     *
     * @param ItemInterface $item
     * @param mixed $data
     */
    public function add(ItemInterface $item, $data = null): void
    {
        $this->attach($item, $data);
    }

    /**
     * Checks that an Item is already in the collection
     *
     * @param ItemInterface $item
     * @return bool
     */
    public function containing(ItemInterface $item): bool
    {
        return $this->contains($item);
    }

    /**
     * Remove item from the collection
     *
     * @param ItemInterface $item
     */
    public function remove(ItemInterface $item): void
    {
        $this->detach($item);
    }

    /**
     * Make Union with a given collection and the result would be the base Collection having all of the items
     *
     * @param Collection|CollectionInterface $collection
     */
    public function unionWith(CollectionInterface $collection): void
    {
        $this->addAll($collection);
    }

    /**
     * Subtract items from the collections using an other collection
     *
     * @param Collection|CollectionInterface $collection
     */
    public function subtractWith(CollectionInterface $collection): void
    {
        $this->removeAll($collection);
    }

    /**
     * Subtract items from the collection but keeping the given collection items if they are in the base collection
     *
     * @param Collection|CollectionInterface $collection
     */
    public function intersectWith(CollectionInterface $collection): void
    {
        $this->removeAllExcept($collection);
    }

    /**
     * By default the hash is the Entity ID
     *
     * @param ItemInterface $item
     * @return string
     */
    public function getHash($item): string
    {
        $this->items[$item->id()] =& $item;
        return "{$item->id()}";
    }
}
