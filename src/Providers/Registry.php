<?php
namespace SkyBet\TechDemo\Providers;

use SkyBet\TechDemo\Abstracts\Registry as RegistryInterface;
use SkyBet\TechDemo\Abstracts\DataSource as DataSourceInterface;
use SkyBet\TechDemo\Abstracts\Collection as CollectionInterface;
use SkyBet\TechDemo\Abstracts\Entity as EntityInterface;

/**
 * Simple provider for Registry classes
 */
abstract class Registry implements RegistryInterface
{
    /** @var DataSourceInterface */
    protected $db;

    /**
     * Get dependencies and initialize
     *
     * @param DataSourceInterface $dataSource
     * @param string $sourceName
     */
    public function __construct(DataSourceInterface $dataSource, $sourceName)
    {
        $this->db = $dataSource;
        $this->db->open($sourceName);
    }

    /**
     * Basic query for an Entity
     *
     * @param int $id
     * @return EntityInterface
     */
    public abstract function getById(int $id): EntityInterface;

    /**
     * Get all Results
     *
     * @return CollectionInterface
     */
    public abstract function getAll(): CollectionInterface;
}
