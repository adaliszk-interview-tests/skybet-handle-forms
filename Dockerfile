FROM phpearth/php:7.1-litespeed

RUN apk add --no-cache composer phpunit

ADD .:/var/www/html/

EXPOSE 80