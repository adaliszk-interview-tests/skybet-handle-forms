Simple Form handle MicroService demo
===

The aim of this demo is to create a server-side form handler in the language of your choice,
to load and save data from our supplied HTML form. The main functionality required is a simple
update capability against the existing records. In order to validate this functionality, you
should include an appropriate level of test coverage.

For a datastore, the application is read and write from the file on disk, rather than using a
relational/noSQL solution.