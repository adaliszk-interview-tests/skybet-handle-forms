<?php
namespace SkyBet\TechDemo\Api\Tests;

use Laravel\Lumen\Testing\TestCase as TestCasePresenter;

class TestCase extends TestCasePresenter
{
    /**
     * Creates the application.
     *
     * Needs to be implemented by subclasses.
     *
     * @return \Symfony\Component\HttpKernel\HttpKernelInterface
     */
    public function createApplication()
    {
        // TODO: Implement createApplication() method.
    }
}
